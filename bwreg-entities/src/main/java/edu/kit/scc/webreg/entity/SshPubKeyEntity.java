package edu.kit.scc.webreg.entity;

import java.sql.Types;
import java.util.Date;
import java.util.Set;

import org.hibernate.annotations.JdbcTypeCode;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "SshPubKeyEntity")
@Table(name = "ssh_pub_key")
public class SshPubKeyEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

	@OneToMany(targetEntity = SshPubKeyRegistryEntity.class, mappedBy = "sshPubKey")
	private Set<SshPubKeyRegistryEntity> sshPubKeyRegistries;

	@Enumerated(EnumType.STRING)
	private SshPubKeyStatus keyStatus;

	@Column(name = "name", length = 128)
	private String name;
	
	@Column(name = "key_type", length = 32)
	private String keyType;

	@Column(name = "encoded_key")
	@Lob 
	@JdbcTypeCode(Types.LONGVARCHAR)	
	private String encodedKey;

	@Column(name = "comment", length = 1024)
	private String comment;	

	@Column(name = "expires_at")
	private Date expiresAt;

	@Column(name = "expires_warn_sent_at")
	private Date expireWarningSent;

	@Column(name = "expired_sent_at")
	private Date expiredSent;

}
