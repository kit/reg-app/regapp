package edu.kit.scc.webreg.entity.attribute.value;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "LongValueEntity")
public class LongValueEntity extends ValueEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "value_long")
	private Long valueLong;
	
}
