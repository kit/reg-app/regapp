package edu.kit.scc.webreg.entity;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "SamlUserEntity")
public class SamlUserEntity extends UserEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "persistent_id", length = 1024)
	private String persistentId;

	@Column(name = "saml_subject_id", length = 1024)
	private String subjectId;

	@Column(name = "persistent_spid", length = 1024)
	private String persistentSpId;

	@Column(name = "attr_src_id", length = 1024)
	private String attributeSourcedId;

	@Column(name = "attr_src_id_name", length = 1024)
	private String attributeSourcedIdName;

	@ManyToOne(targetEntity = SamlIdpMetadataEntity.class, fetch = FetchType.LAZY)
	private SamlIdpMetadataEntity idp;
	
	@OneToMany(targetEntity = SamlAssertionEntity.class, mappedBy="user")
	private Set<SamlAssertionEntity> assertions;
}
