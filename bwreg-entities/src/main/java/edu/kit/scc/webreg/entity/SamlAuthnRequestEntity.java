package edu.kit.scc.webreg.entity;

import java.sql.Types;
import java.util.Date;

import org.hibernate.annotations.JdbcTypeCode;

import edu.kit.scc.webreg.entity.attribute.AttributeReleaseEntity;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "SamlAuthnRequestEntity")
@Table(name = "samlauthnrequest")
public class SamlAuthnRequestEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "authnrequest_data", columnDefinition="TEXT")
	@Basic(fetch = FetchType.LAZY)
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)	
	private String authnrequestData;

	@ManyToOne(targetEntity = AttributeReleaseEntity.class)
	private AttributeReleaseEntity attributeRelease;

	@ManyToOne(targetEntity = SamlSpMetadataEntity.class)
	private SamlSpMetadataEntity spMetadata;
	
	@Column(name = "valid_until")
	private Date validUntil;
}
