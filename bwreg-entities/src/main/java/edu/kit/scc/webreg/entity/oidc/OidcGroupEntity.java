package edu.kit.scc.webreg.entity.oidc;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import edu.kit.scc.webreg.entity.ServiceBasedGroupEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcGroupEntity")
public class OidcGroupEntity extends ServiceBasedGroupEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = OidcRpConfigurationEntity.class)
	private OidcRpConfigurationEntity issuer;

	@Column(name = "group_prefix")
	private String prefix;

}
