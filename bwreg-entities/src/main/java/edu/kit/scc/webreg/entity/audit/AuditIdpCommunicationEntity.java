/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity.audit;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import java.io.Serial;

import edu.kit.scc.webreg.entity.SamlIdpMetadataEntity;
import edu.kit.scc.webreg.entity.SamlSpConfigurationEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "AuditIdpCommunicationEntity")
public class AuditIdpCommunicationEntity extends AuditEntryEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = SamlIdpMetadataEntity.class)
	private SamlIdpMetadataEntity idp;

	@ManyToOne(targetEntity = SamlSpConfigurationEntity.class)
	private SamlSpConfigurationEntity spConfig;

}
