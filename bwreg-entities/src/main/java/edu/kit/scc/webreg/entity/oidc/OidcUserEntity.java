package edu.kit.scc.webreg.entity.oidc;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import edu.kit.scc.webreg.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcUserEntity")
public class OidcUserEntity extends UserEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	@Column(name = "subject_id", length = 1024)
	private String subjectId;
	
	@ManyToOne(targetEntity = OidcRpConfigurationEntity.class)
	private OidcRpConfigurationEntity issuer;

}
