package edu.kit.scc.webreg.entity;

import java.sql.Types;

import org.hibernate.annotations.JdbcTypeCode;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "key_store")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class KeyStoreEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "base64encodedkeystoreblob", columnDefinition="TEXT")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)
	private String base64EncodedKeyStoreBlob;

	@Column(name = "context", length = 255, unique = true)
	private String context;
}
