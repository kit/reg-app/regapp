/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "registry")
public class RegistryEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private RegistryStatus registryStatus;
	
	@Column(name="status_messages", length=1024)
	private String statusMessage;
	
	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;
	
	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;
	
	@ManyToOne(targetEntity = ServiceEntity.class)
	private ServiceEntity service;

	@NotNull
	
	@Column(name="register_bean", length=256, nullable=false)
	private String registerBean;
	
	@Column(name="approval_bean", length=256)
	private String approvalBean;
	
	@ManyToMany(targetEntity=AgreementTextEntity.class)
	@JoinTable(name = "registry_agreementtext",
			joinColumns = @JoinColumn(name="registry_id"),
			inverseJoinColumns = @JoinColumn(name="agreementtext_id")
	)
	private Set<AgreementTextEntity> agreedTexts;
	
	@Column(name="agreed_time")
	private Date agreedTime;
	
	@ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
	@JoinTable(name = "registry_value")
    @MapKeyColumn(name = "key_data", length = 128)
    @Column(name = "value_data", length = 2048)
    private Map<String, String> registryValues; 
	
	@Column(name = "last_recon")
	private Date lastReconcile;
	
	@Column(name = "last_full_recon")
	private Date lastFullReconcile;
	
	@Column(name = "last_status_change")
	private Date lastStatusChange;
	
	@Column(name = "last_access_check")
	private Date lastAccessCheck;	
	
}
