package edu.kit.scc.webreg.entity.attribute.value;

import edu.kit.scc.webreg.entity.attribute.AttributeConsumerEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "PairwiseIdentifierValueEntity")
public class PairwiseIdentifierValueEntity extends ValueEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "value_identifier", length = 4096)
	private String valueIdentifier;

	@Column(name = "value_scope", length = 4096)
	private String valueScope;

	@ManyToOne(targetEntity = AttributeConsumerEntity.class)
	private AttributeConsumerEntity AttributeConsumerEntity;
	
}
