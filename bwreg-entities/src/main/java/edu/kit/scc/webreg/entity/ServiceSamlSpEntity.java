package edu.kit.scc.webreg.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ServiceSamlSpEntity")
@Table(name = "service_saml_sp")
public class ServiceSamlSpEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = ServiceEntity.class)
	private ServiceEntity service;

	@ManyToOne(targetEntity = SamlSpMetadataEntity.class)
	private SamlSpMetadataEntity sp;

	@ManyToOne(targetEntity = SamlIdpConfigurationEntity.class)
	private SamlIdpConfigurationEntity idp;
	
	@ManyToOne (targetEntity = ScriptEntity.class)
	private ScriptEntity script;
	
	@Column(name = "wants_elevation")
	private Boolean wantsElevation;
	
}
