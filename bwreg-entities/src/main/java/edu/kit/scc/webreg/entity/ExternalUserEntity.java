package edu.kit.scc.webreg.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ExternalUserEntity")
public class ExternalUserEntity extends UserEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "external_id", length = 1024)
	private String externalId;
	
	@ManyToOne(targetEntity = ExternalUserAdminRoleEntity.class)
	private ExternalUserAdminRoleEntity admin;
}
