package edu.kit.scc.webreg.entity.attribute.value;

import java.util.List;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "StringListValueEntity")
public class StringListValueEntity extends ValueEntity {

	private static final long serialVersionUID = 1L;

    @ElementCollection
    @CollectionTable(
        name="value_string_list",
        joinColumns = @JoinColumn(name="value_id")
    )
	@Column(name = "value_string", length = 4096)
	private List<String> valueList;

}
