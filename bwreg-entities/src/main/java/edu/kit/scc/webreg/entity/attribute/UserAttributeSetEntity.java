package edu.kit.scc.webreg.entity.attribute;

import edu.kit.scc.webreg.entity.UserEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "UserAttributeSetEntity")
public class UserAttributeSetEntity extends AttributeSetEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;

}
