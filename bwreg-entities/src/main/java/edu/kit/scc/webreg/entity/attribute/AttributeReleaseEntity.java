package edu.kit.scc.webreg.entity.attribute;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import edu.kit.scc.webreg.entity.attribute.value.ValueEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "AttributeReleaseEntity")
@Table(name = "attribute_release")
public class AttributeReleaseEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "issued_at")
	protected Date issuedAt;

	@Column(name = "valid_until")
	protected Date validUntil;

	@ManyToOne(targetEntity = AttributeConsumerEntity.class)
	private AttributeConsumerEntity attributeConsumer;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

	@Enumerated(EnumType.STRING)
    @Column(name = "release_status")
	private ReleaseStatusType releaseStatus;

	@OneToMany(mappedBy = "attributeRelease")
	private Set<ValueEntity> values = new HashSet<>(); 
	
	@Transient
	private Boolean changed;

	@Transient
	private Set<ValueEntity> valuesToDelete;	

}
