/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBaseEntity implements BaseEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	protected Long id;
	
	@Column(name = "created_at")
	protected Date createdAt;

	@Column(name = "updated_at")
	protected Date updatedAt;
	
	@Column(name = "version", columnDefinition = "integer DEFAULT 0", nullable = false)
	@Version
	protected Integer version = 0;
	
	public boolean equals(Object other) {
		if (other == null) return false;
		
	    return this.getClass().equals(other.getClass()) && 
	    		(getId() != null) 
	         ? getId().equals(((BaseEntity) other).getId()) 
	         : (other == this);
	}

	public int hashCode() {
	    return (getId() != null) 
	         ? (getClass().hashCode() + getId().hashCode())
	         : super.hashCode();
	}

	@PrePersist
	public void prePersist() {
		Date d = new Date();
		setCreatedAt(d);
		setUpdatedAt(d);
	}

	@PreUpdate
	public void preUpdate() {
		setUpdatedAt(new Date());
	}
}
