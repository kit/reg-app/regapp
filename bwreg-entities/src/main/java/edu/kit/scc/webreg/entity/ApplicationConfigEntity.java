package edu.kit.scc.webreg.entity;

import java.util.Date;
import java.util.Map;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinTable;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ApplicationConfigEntity")
@Table(name = "application_config")
public class ApplicationConfigEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "format_version", length = 128)
	private String configFormatVersion;
	
	@Column(name = "sub_version", length = 128)
	private String subVersion;
	
	@Column(name = "activeConfig")
	private Boolean activeConfig;

	@Column(name = "dirty_stamp")
	private Date dirtyStamp;

	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name = "application_config_options")
    @MapKeyColumn(name = "key_data", length = 128)
    @Column(name = "value_data", length = 2048)
    private Map<String, String> configOptions;
}
