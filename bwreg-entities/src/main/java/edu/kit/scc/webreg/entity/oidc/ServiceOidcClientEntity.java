package edu.kit.scc.webreg.entity.oidc;

import java.io.Serial;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import edu.kit.scc.webreg.entity.BusinessRulePackageEntity;
import edu.kit.scc.webreg.entity.ScriptEntity;
import edu.kit.scc.webreg.entity.ServiceEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ServiceOidcClientEntity")
@Table(name = "service_oidc_client")
public class ServiceOidcClientEntity extends AbstractBaseEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = ServiceEntity.class)
	private ServiceEntity service;

	@ManyToOne(targetEntity = OidcClientConfigurationEntity.class)
	private OidcClientConfigurationEntity clientConfig;

	@ManyToOne(targetEntity = ScriptEntity.class)
	private ScriptEntity script;
	
	@ManyToOne(targetEntity = BusinessRulePackageEntity.class)
	private BusinessRulePackageEntity rulePackage;
	
	@Column(name = "wants_elevation")
	private Boolean wantsElevation;
	
	@Column(name = "order_criteria")
	private Integer orderCriteria;

}
