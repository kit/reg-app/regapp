/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity.identity;

import java.util.Date;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import edu.kit.scc.webreg.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "IdentityEntity")
@Table(name = "idty")
public class IdentityEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name="twofa_user_id", length=512, unique = true)
	private String twoFaUserId;

	@Column(name="twofa_user_name", length=512)
	private String twoFaUserName;

	@OneToMany(targetEntity=UserEntity.class, mappedBy = "identity")
	private Set<UserEntity> users;

	@OneToMany(targetEntity=IdentityUserPreferenceEntity.class, mappedBy = "identity")
	private Set<IdentityUserPreferenceEntity> userPrefs;

	@OneToMany(targetEntity=IdentityEmailAddressEntity.class, mappedBy = "identity")
	private Set<IdentityEmailAddressEntity> emailAddresses;

	@ManyToOne(targetEntity = IdentityEmailAddressEntity.class)
    @JoinColumn(name = "email_address_id", nullable = true)
	private IdentityEmailAddressEntity primaryEmail;

	@ManyToOne(targetEntity = UserEntity.class)
    @JoinColumn(name = "pref_user_id")
	private UserEntity prefUser;

	@Column(name = "uid_number")
	private Integer uidNumber;
	
	@Column(name = "generated_local_username", length = 32, unique = true)
	private String generatedLocalUsername;

	@Column(name = "chosen_local_username", length = 32, unique = true)
	private String chosenLocalUsername;

	@Column(name = "registration_lock")
	protected Date registrationLock;

}
