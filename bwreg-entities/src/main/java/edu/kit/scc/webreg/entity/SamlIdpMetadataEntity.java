/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "SamlIdpMetadataEntity")
@Table(name = "idpmetadata")
public class SamlIdpMetadataEntity extends SamlMetadataEntity {

	private static final long serialVersionUID = 1L;

	@ManyToMany(targetEntity = FederationEntity.class,  
			cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(name = "idpmetadata_federation",
			joinColumns = @JoinColumn(name = "idpmetadata_id"),
			inverseJoinColumns = @JoinColumn(name = "federation_id"))	
	private Set<FederationEntity> federations;
	
	@OneToMany(targetEntity = SamlIdpScopeEntity.class, 
			mappedBy = "idp", cascade = CascadeType.REMOVE)
	private Set<SamlIdpScopeEntity> scopes; 

	@OneToMany(targetEntity = SamlIdpMetadataAdminRoleEntity.class, mappedBy = "idp")
	private Set<SamlIdpMetadataAdminRoleEntity> adminRoles; 

	@ElementCollection
	@JoinTable(name = "idp_entity_categories")
	@Column(name = "value_data", length = 2048)
	private List<String> entityCategoryList;
	
	@Enumerated(EnumType.STRING)
	private SamlIdpMetadataEntityStatus aqIdpStatus;
	
	@Column(name = "last_aq_status_change")
	private Date lastAqStatusChange;

	@Enumerated(EnumType.STRING)
	private SamlIdpMetadataEntityStatus idIdpStatus;
	
	@Column(name = "last_id_status_change")
	private Date lastIdStatusChange;	
}
