package edu.kit.scc.webreg.entity.oauth;

import edu.kit.scc.webreg.entity.UserEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OAuthUserEntity")
public class OAuthUserEntity extends UserEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "oauth_id", length = 1024)
	private String oauthId;
	
	@ManyToOne(targetEntity = OAuthRpConfigurationEntity.class)
	private OAuthRpConfigurationEntity oauthIssuer;

}
