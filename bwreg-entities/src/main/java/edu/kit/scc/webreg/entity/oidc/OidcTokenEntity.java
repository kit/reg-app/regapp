package edu.kit.scc.webreg.entity.oidc;

import java.sql.Types;

import org.hibernate.annotations.JdbcTypeCode;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcTokenEntity")
@Table(name = "oidctoken")
public class OidcTokenEntity extends AbstractBaseEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;
	
	@Column(name = "id_token_data")
	@Basic(fetch = FetchType.LAZY)
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)	
	private String idTokenData;

	@Column(name = "user_info_data")
	@Basic(fetch = FetchType.LAZY)
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)	
	private String userInfoData;

	@ManyToOne(targetEntity = OidcUserEntity.class)
	private OidcUserEntity user;

}
