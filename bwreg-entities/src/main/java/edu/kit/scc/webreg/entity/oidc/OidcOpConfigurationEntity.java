package edu.kit.scc.webreg.entity.oidc;

import java.sql.Types;

import org.hibernate.annotations.JdbcTypeCode;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcOpConfigurationEntity")
@Table(name = "oidc_op_configuration")
public class OidcOpConfigurationEntity extends AbstractBaseEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 64)
	private String name;

	@Column(name = "realm", length = 64)
	private String realm;

	@Column(name = "host", length = 256)
	private String host;

	@Column(name = "scope", length = 1024)
	private String scope;

	@Column(name = "private_key")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)		
	private String privateKey;
	
	@Column(name = "certificate")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)		
	private String certificate;
	
	@Column(name = "standby_private_key")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)		
	private String standbyPrivateKey;
	
	@Column(name = "standby_certificate")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)		
	private String standbyCertificate;
	
	@Enumerated(EnumType.STRING)
	private OidcOpConfigurationStatusType opStatus; 

}
