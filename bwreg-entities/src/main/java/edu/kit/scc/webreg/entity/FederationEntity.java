/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import java.util.Date;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "FederationEntity")
@Table(name = "federation")
public class FederationEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "federation_name", length = 128, unique = true)
	private String name;

	@Column(name = "short_name", length = 128, unique = true)
	private String shortName;

	@Column(name = "entity_id", length = 2048)
	private String entityId;

	@Column(name = "federation_metadata_url", length = 2048)
	private String federationMetadataUrl;

	@Column(name = "entity_category_filter", length = 512)
	private String entityCategoryFilter;

	@ManyToOne(targetEntity = BusinessRulePackageEntity.class)
	private BusinessRulePackageEntity entityFilterRulePackage;

	@Column(name = "polled_at")
	private Date polledAt;
	
	@Column(name = "fetch_idps")
	private Boolean fetchIdps;

	@Column(name = "fetch_sps")
	private Boolean fetchSps;

	@Column(name = "fetch_aas")
	private Boolean fetchAAs;
	
	@ManyToOne(targetEntity = ImageEntity.class)
	private ImageEntity logoImage;
	
	@ManyToMany(targetEntity = SamlIdpMetadataEntity.class, mappedBy="federations")
	private Set<SamlIdpMetadataEntity> idps;

	@ManyToMany(targetEntity = SamlSpMetadataEntity.class, mappedBy="federations")
	private Set<SamlSpMetadataEntity> sps;

	@ManyToMany(targetEntity = SamlAAMetadataEntity.class, mappedBy="federations")
	private Set<SamlAAMetadataEntity> aas;

	@PostLoad
	public void postLoad() {
		// Populate standard values if null, which is the case for updated webreg
		if (fetchIdps == null)
			fetchIdps = Boolean.TRUE;
		if (fetchSps == null)
			fetchSps = Boolean.FALSE;
		if (fetchAAs == null)
			fetchAAs = Boolean.FALSE;
	}
}
