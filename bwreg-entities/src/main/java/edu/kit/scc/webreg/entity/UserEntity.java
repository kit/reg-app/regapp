/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import edu.kit.scc.webreg.entity.as.ASUserAttrEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "UserEntity")
@Table(name = "usertable")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UserEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "eppn", length = 1024)
	private String eppn;
	
	@Column(name = "email", length = 1024)
	private String email;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name = "user_email_addresses")
	@Column(name = "emailaddresses", length = 1024)
	private Set<String> emailAddresses;
	
	@Column(name = "given_name", length = 256)
	private String givenName;
	
	@Column(name = "sur_name", length = 256)
	private String surName;

	@Column(name = "name", length = 1024)
	private String name;

	@Column(name = "last_login_host", length = 1024)
	private String lastLoginHost;

	@Column(name = "uid_number", unique = true, nullable = false)
	private Integer uidNumber;
	
	@OneToMany(targetEntity=UserRoleEntity.class, mappedBy = "user")
	private Set<UserRoleEntity> roles;
	
	@ElementCollection
	@JoinTable(name = "user_store")
    @MapKeyColumn(name = "key_data", length = 128)
    @Column(name = "value_data", length = 2048)
    private Map<String, String> genericStore; 
	
	@ElementCollection
	@JoinTable(name = "user_attribute_store")
    @MapKeyColumn(name = "key_data", length = 1024)
    @Column(name = "value_data", length = 4096)
    private Map<String, String> attributeStore; 

	@OneToMany(targetEntity = UserGroupEntity.class, mappedBy="user")
	private Set<UserGroupEntity> groups;

	@OneToMany(targetEntity = ASUserAttrEntity.class, mappedBy="user")
	private Set<ASUserAttrEntity> userAttrs;
		
	@OneToMany(targetEntity = SshPubKeyEntity.class, mappedBy="user")
	private Set<SshPubKeyEntity> sshPubKeys;
		
	@OneToMany(targetEntity = RegistryEntity.class, mappedBy="user")
	private Set<RegistryEntity> registries;
		
	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	@Column(name = "scheduled_update")
	private Date scheduledUpdate;
	
	@Column(name = "last_failed_update")
	private Date lastFailedUpdate;
	
	@Column(name = "expires_warn_sent_at")
	private Date expireWarningSent;

	@Column(name = "expired_sent_at")
	private Date expiredSent;
	
	@Column(name = "theme", length = 128)
	private String theme;
	
	@Column(name = "locale", length = 64)
	private String locale;
	
	@ManyToOne(targetEntity = GroupEntity.class)
	private GroupEntity primaryGroup;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

	@Column(name = "last_status_change")
	private Date lastStatusChange;
		
}
