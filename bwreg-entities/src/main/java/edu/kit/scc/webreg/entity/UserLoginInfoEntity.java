package edu.kit.scc.webreg.entity;

import java.util.Date;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "UserLoginInfoEntity")
@Table(name = "user_login_info")
public class UserLoginInfoEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

	@ManyToOne(targetEntity = RegistryEntity.class)
	private RegistryEntity registry;

	@Enumerated(EnumType.STRING)
	private UserLoginInfoStatus loginStatus;

	@Column(name = "login_date")
	private Date loginDate;

	@Column(name = "login_from", length = 256)
	private String from;

	@Enumerated(EnumType.STRING)
	private UserLoginMethod loginMethod;

}
