/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "JobScheduleEntity")
@Table(name="job_schedule")
public class JobScheduleEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", nullable = false, length = 128)
	private String name;

	@ManyToOne(targetEntity = JobClassEntity.class)
	private JobClassEntity jobClass;
	
	@Column(name = "schedule_second", length = 32)
	private String second;
	
	@Column(name = "schedule_minute", length = 32)
	private String minute;
	
	@Column(name = "schedule_hour", length = 32)
	private String hour;
	
	@Column(name = "schedule_month", length = 32)
	private String month;
	
	@Column(name = "schedule_year", length = 32)	
	private String year;
	
	@Column(name = "schedule_dow", length = 32)
	private String dayOfWeek;
	
	@Column(name = "schedule_dom", length = 32)
	private String dayOfMonth;

	@Column(name = "diabled")
	private Boolean disabled;	
}
