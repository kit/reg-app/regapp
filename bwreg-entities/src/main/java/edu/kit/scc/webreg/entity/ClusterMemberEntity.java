package edu.kit.scc.webreg.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ClusterMemberEntity")
@Table(name = "cluster_member")
public class ClusterMemberEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "node_name", length = 128)
	private String nodeName;

	@Enumerated(EnumType.STRING)
	private ClusterMemberStatus clusterMemberStatus;
	
	@Column(name = "last_status_change")
	private Date lastStatusChange;
	
	@Column(name = "last_status_check")
	private Date lastStatusCheck;
	
	@Enumerated(EnumType.STRING)
	private ClusterSchedulerStatus clusterSchedulerStatus;
	
	@Column(name = "last_scheduler_status_change")
	private Date lastSchedulerStatusChange;	
}
