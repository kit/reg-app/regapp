package edu.kit.scc.webreg.entity.oauth;

import java.util.Map;

import edu.kit.scc.webreg.entity.UserProvisionerEntity;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinTable;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OAuthRpConfigurationEntity")
@Table(name = "oauth_rp_configuration")
public class OAuthRpConfigurationEntity extends UserProvisionerEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 64)
	private String name;

	@Column(name = "client_id", length = 512)
	private String clientId;

	@Column(name = "scopes", length = 1024)
	private String scopes;

	@Column(name = "secret", length = 512)
	private String secret;

	@Column(name = "service_url", length = 1024)
	private String serviceUrl;

	@Column(name = "callback_url", length = 1024)
	private String callbackUrl;

	@Column(name = "logo_url", length = 1024)
	private String logoUrl;

	@Column(name = "logo_small_url", length = 1024)
	private String logoSmallUrl;

	@ElementCollection
	@JoinTable(name = "oauth_rp_configuration_generic_store")
    @MapKeyColumn(name = "key_data", length = 128)
    @Column(name = "value_data", length = 2048)
    private Map<String, String> genericStore; 

}
