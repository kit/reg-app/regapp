package edu.kit.scc.webreg.entity;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ServiceBasedGroupEntity")
public class ServiceBasedGroupEntity extends GroupEntity {

	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy = "group", targetEntity = ServiceGroupFlagEntity.class)
	private Set<ServiceGroupFlagEntity> serviceGroupFlags;
}
