package edu.kit.scc.webreg.entity.oauth;

import edu.kit.scc.webreg.entity.ServiceBasedGroupEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import java.io.Serial;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OAuthGroupEntity")
public class OAuthGroupEntity extends ServiceBasedGroupEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = OAuthRpConfigurationEntity.class)
	private OAuthRpConfigurationEntity oauthIssuer;

	@Column(name = "group_prefix")
	private String prefix;

}
