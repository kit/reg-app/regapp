package edu.kit.scc.webreg.entity.oidc;

import java.util.Set;

import edu.kit.scc.webreg.entity.attribute.AttributeConsumerEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcClientConsumerEntity")
@Table(name = "oidc_client_consumer")
public class OidcClientConsumerEntity extends AttributeConsumerEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 64)
	private String name;

	@Column(name = "secret", length = 256)
	private String secret;

	@Column(name = "displayName", length = 1024)
	private String displayName;

	@ManyToOne(targetEntity = OidcOpConfigurationEntity.class)
	private OidcOpConfigurationEntity opConfiguration;

	@Column(name = "public_client")
	private Boolean publicClient;
	
	@OneToMany(mappedBy = "client")
	Set<OidcRedirectUrlEntity> redirects;

}
