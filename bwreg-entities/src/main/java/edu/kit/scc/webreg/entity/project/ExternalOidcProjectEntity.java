package edu.kit.scc.webreg.entity.project;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import edu.kit.scc.webreg.entity.oidc.OidcRpConfigurationEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ExternalOidcProjectEntity")
public class ExternalOidcProjectEntity extends ExternalProjectEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = OidcRpConfigurationEntity.class)
	private OidcRpConfigurationEntity rpConfig;

}
