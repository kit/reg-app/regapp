package edu.kit.scc.webreg.entity.oidc;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import edu.kit.scc.webreg.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcRpFlowStateEntity")
@Table(name = "oidc_rp_flow_state")
public class OidcRpFlowStateEntity extends AbstractBaseEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;

	@ManyToOne(targetEntity = OidcRpConfigurationEntity.class)
	private OidcRpConfigurationEntity rpConfiguration;

	@Column(name = "state", length = 256)
	private String state;

	@Column(name = "code", length = 256)
	private String code;

	@Column(name = "nonce", length = 256)
	private String nonce;

	@Column(name = "valid_until")
	private Date validUntil;

}
