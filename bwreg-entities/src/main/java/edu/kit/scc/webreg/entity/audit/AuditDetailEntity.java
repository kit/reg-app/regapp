/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity.audit;

import java.io.Serial;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "AuditDetailEntity")
@Table(name = "audit_detail")
public class AuditDetailEntity extends AbstractBaseEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = AuditEntryEntity.class)
	private AuditEntryEntity auditEntry;
	
	@Column(name = "end_time")
	private Date endTime;
	
	@Enumerated(EnumType.STRING)
	private AuditStatus auditStatus;

	@Column(name = "audit_subject", length = 256)
	private String subject;
	
	@Column(name = "audit_action", length = 128)
	private String action;
	
	@Column(name = "audit_object", length = 256)
	private String object;
	
	@Column(name = "audit_log", length = 1024)
	private String log;

}
