/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity.project;

import java.sql.Types;
import java.util.Date;
import java.util.Set;

import org.hibernate.annotations.JdbcTypeCode;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectEntity")
@Table(name = "project")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ProjectEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name="name", length=512, nullable=false)
	private String name;
	
	@NotNull
	@Column(name="short_name", length=32, nullable=false, unique=true)
	@Pattern(regexp = "^[a-z]{1}[a-z0-9-_]{0,31}$")	
	private String shortName;
	
	@NotNull
	@Column(name="group_name", length=64, nullable=false, unique=true)
	@Pattern(regexp = "^[a-z]{1}[a-z0-9-_]{0,63}$")
	private String groupName;
	
    @ManyToOne(targetEntity = ProjectEntity.class)
	private ProjectEntity parentProject;
	
	@OneToMany(mappedBy = "parentProject")
	private Set<ProjectEntity> childProjects;

	@Column(name = "description")
	@Lob
	@JdbcTypeCode(Types.LONGVARCHAR)
	private String description;
	
	@Column(name = "short_description", length = 2048)
	private String shortDescription;

	@Column(name = "sub_projects_allowed")
	private Boolean subProjectsAllowed;

	@Column(name = "published")
	private Boolean published;
	
	@Column(name = "approved")
	private Boolean approved;

	@Column(name = "attribute_prefix", length = 2048)
	private String attributePrefix;
	
	@Column(name = "attribute_name", length = 256)
	private String attributeName;
	
	@Column(name="projectValidity")
	private Date projectValidity;
	
	@Enumerated(EnumType.STRING)
	private ProjectStatus projectStatus;	

	@NotNull
	@OneToOne(targetEntity = LocalProjectGroupEntity.class)
    @JoinColumn(name = "group_id", nullable = false)
	private LocalProjectGroupEntity projectGroup;
	
	@OneToMany(mappedBy = "project")
	private Set<ProjectServiceEntity> projectServices;

	@OneToMany(mappedBy = "project")
	private Set<ProjectIdentityAdminEntity> projectAdmins;

	@Column(name = "last_sync_to_group")
	private Date lastSyncToGroup;

}
