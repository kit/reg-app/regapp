package edu.kit.scc.webreg.entity.project;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import edu.kit.scc.webreg.entity.as.AttributeSourceEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "AttributeSourceProjectEntity")
public class AttributeSourceProjectEntity extends ExternalProjectEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = AttributeSourceEntity.class)
	private AttributeSourceEntity attributeSource;
}
