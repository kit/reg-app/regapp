package edu.kit.scc.webreg.entity;

import java.util.Date;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "SshPubKeyRegistryEntity")
@Table(name = "ssh_pub_key_registry")
public class SshPubKeyRegistryEntity extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne(targetEntity = RegistryEntity.class)
	private RegistryEntity registry;
	
	@ManyToOne(targetEntity = SshPubKeyEntity.class)
	private SshPubKeyEntity sshPubKey;

	@Enumerated(EnumType.STRING)
	private SshPubKeyRegistryStatus keyStatus;

	@Enumerated(EnumType.STRING)
	private SshPubKeyUsageType usageType;

	@Column(name = "command", length = 1024)
	private String command;
	
	@Column(name = "ssh_from", length = 1024)
	private String from;

	@Column(name = "comment", length = 1024)
	private String comment;	

	@Column(name = "approver_comment", length = 2048)
	private String approverComment;	

	@Column(name = "approved_at")
	private Date approvedAt;	

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity approver;

	@Column(name = "expires_at")
	private Date expiresAt;
	
}
