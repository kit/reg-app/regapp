package edu.kit.scc.webreg.entity.attribute;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "IdentityAttributeSetEntity")
public class IdentityAttributeSetEntity extends AttributeSetEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

}
