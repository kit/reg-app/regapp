/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.entity.audit;

import java.io.Serial;
import java.util.Date;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name  = "AuditEntryEntity")
@Table(name = "audit_entry")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AuditEntryEntity extends AbstractBaseEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@Column(name = "start_time")
	private Date startTime;
	
	@Column(name = "end_time")
	private Date endTime;
	
	@Column(name = "audit_name", length=256)
	private String name;
	
	@Column(name = "audit_detail", length=1024)
	private String detail;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE },
			targetEntity = AuditDetailEntity.class, mappedBy = "auditEntry")
	private Set<AuditDetailEntity> auditDetails;

	@Column(name = "audit_executor", length=64)
	private String executor;

	@ManyToOne(targetEntity = AuditEntryEntity.class)
	private AuditEntryEntity parentEntry;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE },
			targetEntity = AuditEntryEntity.class, mappedBy = "parentEntry")
	private Set<AuditEntryEntity> childEntries;

}
