package edu.kit.scc.webreg.entity.oidc;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import edu.kit.scc.webreg.entity.RegistryEntity;
import edu.kit.scc.webreg.entity.UserEntity;
import edu.kit.scc.webreg.entity.attribute.AttributeReleaseEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcFlowStateEntity")
@Table(name = "oidc_flow_state")
public class OidcFlowStateEntity extends AbstractBaseEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(targetEntity = UserEntity.class)
	private UserEntity user;

	@ManyToOne(targetEntity = IdentityEntity.class)
	private IdentityEntity identity;

	@ManyToOne(targetEntity = OidcOpConfigurationEntity.class)
	private OidcOpConfigurationEntity opConfiguration;

	@ManyToOne(targetEntity = OidcClientConfigurationEntity.class)
	private OidcClientConfigurationEntity clientConfiguration;

	@ManyToOne(targetEntity = OidcClientConsumerEntity.class)
	private OidcClientConsumerEntity clientConsumer;

	@ManyToOne(targetEntity = RegistryEntity.class)
	private RegistryEntity registry;

	@ManyToOne(targetEntity = AttributeReleaseEntity.class)
	private AttributeReleaseEntity attributeRelease;

	@Column(name = "nonce", length = 256)
	private String nonce;

	@Column(name = "state", length = 256)
	private String state;

	@Column(name = "code", length = 256)
	private String code;

	@Column(name = "code_challange", length = 512)
	private String codeChallange;

	@Column(name = "code_challange_method", length = 64)
	private String codecodeChallangeMethod;

	@Column(name = "response_type", length = 256)
	private String responseType;

	@Column(name = "redirect_uri", length = 1024)
	private String redirectUri;
	
	@Column(name = "access_token", length = 4096)
	private String accessToken;

	@Column(name = "refresh_token", length = 4096)
	private String refreshToken;

	@Column(name = "scope", length = 4096)
	private String scope;

	@Column(name = "acr_values", length = 4096)
	private String acrValues;

	@Column(name = "access_token_type", length = 32)
	private String accessTokenType;

	@Column(name = "valid_until")
	private Date validUntil;

}
