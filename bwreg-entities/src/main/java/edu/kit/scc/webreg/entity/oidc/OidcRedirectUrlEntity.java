package edu.kit.scc.webreg.entity.oidc;

import edu.kit.scc.webreg.entity.AbstractBaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "OidcRedirectUrlEntity")
@Table(name = "oidc_client_redirect_url")
public class OidcRedirectUrlEntity extends AbstractBaseEntity {

	@java.io.Serial
	private static final long serialVersionUID = 1L;
	
	@Column(name = "redirect_url", length = 2048)
	private String url;

	@ManyToOne(targetEntity = OidcClientConsumerEntity.class)
	private OidcClientConsumerEntity client;

}
