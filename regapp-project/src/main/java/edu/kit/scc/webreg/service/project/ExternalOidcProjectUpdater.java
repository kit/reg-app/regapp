package edu.kit.scc.webreg.service.project;

import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.slf4j.Logger;

import edu.kit.scc.webreg.bootstrap.ApplicationConfig;
import edu.kit.scc.webreg.dao.project.BaseProjectDao;
import edu.kit.scc.webreg.dao.project.ExternalOidcProjectDao;
import edu.kit.scc.webreg.entity.oidc.OidcRpConfigurationEntity;
import edu.kit.scc.webreg.entity.oidc.OidcUserEntity;
import edu.kit.scc.webreg.entity.project.ExternalOidcProjectEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipType;

@ApplicationScoped
public class ExternalOidcProjectUpdater extends AbstractProjectUpdater<ExternalOidcProjectEntity> {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger logger;
	
	@Inject
	private ExternalOidcProjectDao dao;
	
	@Inject
	private ExternalOidcProjectCreater projectCreater;
	
	@Inject
	private ApplicationConfig appConfig;
	
	public void syncExternalOidcProject(String projectName, String externalName, String groupName, String shortName, OidcUserEntity user) {
		OidcRpConfigurationEntity rpConfig = user.getIssuer();
		
		logger.debug("Inspecting {}", projectName);
		ExternalOidcProjectEntity project = dao.findByExternalNameOidc(externalName, rpConfig);
		
		if (shortName == null) {
			// generate short name, if none is set
			shortName = "p_" + (UUID.randomUUID().toString().replaceAll("-", "").substring(0, 24));
		}
		
		if (project == null) {
			project = projectCreater.create(projectName, externalName, groupName, shortName, rpConfig);
		}

		project.setName(projectName);
		project.setGroupName(groupName);
		project.getProjectGroup().setName(groupName);
		project.setShortName(shortName);
		project.setRpConfig(rpConfig);
		
		if (dao.findByIdentityAndProject(user.getIdentity(), project) == null) {
			
			ProjectMembershipEntity pme = new ProjectMembershipEntity();
			pme.setMembershipType(ProjectMembershipType.MEMBER);
			pme.setMembershipValidity(getMemberShipValdity());
			
			dao.addMemberToProject(project, user.getIdentity(), pme);
		}
		
		syncAllMembersToGroup(project, "idty-" + user.getIdentity());
		triggerGroupUpdate(project, "idty-" + user.getIdentity());
	}
	
	
	public Date getMemberShipValdity() {

		String membershipValidityFromAppConfig = appConfig.getConfigValue("membership_validity");
		String regex = "^\\d{11}$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(membershipValidityFromAppConfig);

		if (matcher.matches()) {
			long membershipValidityMs;
			try {
				membershipValidityMs = Long.parseLong(membershipValidityFromAppConfig);
				return (new Date(System.currentTimeMillis() + membershipValidityMs));

			} catch (NumberFormatException e) {
				throw new IllegalArgumentException(
						"Invalid format for membership_validity in config: " + membershipValidityFromAppConfig, e);
			}
		} else
			return null;

	}

	@Override
	protected BaseProjectDao<ExternalOidcProjectEntity> getDao() {
		return dao;
	}
}
