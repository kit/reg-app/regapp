/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.bean.project;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import jakarta.faces.event.ComponentSystemEvent;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Getter;
import lombok.Setter;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectAdminType;
import edu.kit.scc.webreg.entity.project.ProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectIdentityAdminEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipType;
import edu.kit.scc.webreg.entity.project.ProjectServiceEntity;
import edu.kit.scc.webreg.exc.NotAuthorizedException;
import edu.kit.scc.webreg.service.project.LocalProjectService;
import edu.kit.scc.webreg.service.project.ProjectService;
import edu.kit.scc.webreg.session.SessionManager;
import edu.kit.scc.webreg.util.FacesMessageGenerator;
import edu.kit.scc.webreg.util.LoadFromAppConfig;

@Named
@ViewScoped
public class UserShowLocalProjectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SessionManager session;

	@Inject
	private LocalProjectService service;

	@Inject
	private ProjectService projectService;

	@Setter
	private LocalProjectEntity entity;

	@Inject
	private FacesMessageGenerator messageGenerator;
	
	@Inject
	private LoadFromAppConfig config;

	@Getter
	@Setter
	private String applyReason;
	
	@Getter
	@Setter
	private String declineReason;
	

	private List<ProjectMembershipEntity> memberList;
	private List<ProjectMembershipEntity> effectiveMemberList;
	private List<ProjectIdentityAdminEntity> adminList;
	private List<ProjectServiceEntity> serviceList;
	private List<ProjectServiceEntity> serviceFromParentsList;

	private ProjectIdentityAdminEntity adminIdentity;

	@Getter
	@Setter
	private Long id;

	public void preRenderView(ComponentSystemEvent ev) {
		for (ProjectIdentityAdminEntity a : getAdminList()) {
			if (a.getIdentity().getId().equals(session.getIdentityId())) {
				adminIdentity = a;
				break;
			}
		}

		if (adminIdentity == null) {
			throw new NotAuthorizedException("Nicht autorisiert");
		} else {
			if (!(ProjectAdminType.ADMIN.equals(adminIdentity.getType())
					|| ProjectAdminType.OWNER.equals(adminIdentity.getType()))) {
				throw new NotAuthorizedException("Nicht autorisiert");
			}
		}
	}

	public LocalProjectEntity getEntity() {
		if (entity == null) {
			entity = service.findByIdWithAttrs(id, LocalProjectEntity_.projectServices, ProjectEntity_.childProjects);
		}
		
		return entity;
	}

	public void deleteMember(ProjectMembershipEntity pme) {
		projectService.removeProjectMember(pme, "idty-" + session.getIdentityId());
		entity = null;
		memberList = null;
		effectiveMemberList = null;
		
	}

	public void approveAndBecomeAMember(ProjectMembershipEntity pme) {

		if (pme.getMembershipType().equals(ProjectMembershipType.APPLICANT)) {
			pme.setReasonToApproveDenyMembership(getApplyReason());
			pme.setMembershipType(ProjectMembershipType.MEMBER);
			
			if (config.getMemberShipValdity() != 0L)
				pme.setMembershipValidity(new Date(System.currentTimeMillis() + config.getMemberShipValdity()));
			else
				pme.setMembershipValidity(null);			
			
			projectService.updateProjectMemberStatus(pme, "idty-" + session.getIdentityId());					
		}
		
		entity = null;
		memberList = null;
		effectiveMemberList = null;
	}
	
	
	public void declineMembershipRequest(ProjectMembershipEntity pme) {
	
		declineReason = getDeclineReason();
		
		if (declineReason == null || declineReason.isEmpty()) {
			messageGenerator.addResolvedErrorMessage("mandatoy",
					"mandatoy_details", true);
			return;
		}

		if (pme.getMembershipType().equals(ProjectMembershipType.APPLICANT)) {		
			pme.setReasonToApproveDenyMembership(declineReason);
			pme.setMembershipType(ProjectMembershipType.NOTMEMBER);
			
			if (config.getMemberShipValdity() != 0L)
				pme.setMembershipValidity(new Date(System.currentTimeMillis() + config.getMemberShipValdity()));
			else
				pme.setMembershipValidity(null);
			
			projectService.updateProjectMemberStatus(pme, "idty-" + session.getIdentityId());
		}
		entity = null;
		memberList = null;
		effectiveMemberList = null;
	
	}
		
	public String getReason(ProjectMembershipEntity pme) {		
		if (pme != null)
			return pme.getReasonToApplyForMembership();
		return "";
	}
	
	public List<ProjectMembershipEntity> getMemberList() {
		if (memberList == null) {
			memberList = projectService.findMembersForProject(getEntity());
		}
		return memberList;
	}

	public List<ProjectIdentityAdminEntity> getAdminList() {
		if (adminList == null) {
			adminList = projectService.findAdminsForProject(getEntity());
		}
		return adminList;
	}

	public List<ProjectServiceEntity> getServiceList() {
		if (serviceList == null) {
			serviceList = projectService.findServicesForProject(getEntity());
		}
		return serviceList;
	}

	public ProjectIdentityAdminEntity getAdminIdentity() {
		return adminIdentity;
	}

	public List<ProjectServiceEntity> getServiceFromParentsList() {
		if (serviceFromParentsList == null) {
			serviceFromParentsList = projectService.findServicesFromParentsForProject(getEntity());
		}
		return serviceFromParentsList;
	}

	public List<ProjectMembershipEntity> getEffectiveMemberList() {
		if (effectiveMemberList == null) {
			effectiveMemberList = projectService.findMembersForProject(getEntity(), true);
		}
		return effectiveMemberList;
	}
}
