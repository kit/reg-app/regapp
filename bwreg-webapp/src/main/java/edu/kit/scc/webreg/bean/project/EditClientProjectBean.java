/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Haykuhi Musheghyan
 ******************************************************************************/
package edu.kit.scc.webreg.bean.project;

import java.io.IOException;
import java.io.Serializable;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity;
import edu.kit.scc.webreg.entity.project.ProjectEntity;
import edu.kit.scc.webreg.service.project.LocalProjectService;
import edu.kit.scc.webreg.util.ViewIds;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import edu.kit.scc.webreg.util.FacesMessageGenerator;

@Named
@ViewScoped
public class EditClientProjectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private LocalProjectService service;

	@Inject
	private FacesMessageGenerator messageGenerator;

	private LocalProjectEntity entity;

	@Getter
	@Setter
	private Long projectId;

	public void save() {

		Column projectEntityDbColumnAnnotation = null;

		try {
			projectEntityDbColumnAnnotation = ProjectEntity.class.getDeclaredField("name")
					.getDeclaredAnnotation(Column.class);

		} catch (NoSuchFieldException | SecurityException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
			return;
		}

		if (entity.getName().length() > projectEntityDbColumnAnnotation.length()) {
			messageGenerator.addResolvedErrorMessage("failure", "project.local_project.create_edit_name_length_details",
					true);
			return;
		}
		
		entity = service.save(entity);

		if (entity != null) {
			messageGenerator.addResolvedInfoMessage("success", "project.local_project.create_edit_name_success_detail",
					true);
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect(ViewIds.PROJECT_LOCAL_SHOW + "?id=" + entity.getId());
		} catch (IOException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
		}

	}

	public void cancel() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(ViewIds.PROJECT_LOCAL_INDEX);
		} catch (IOException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
		}

	}

	public LocalProjectEntity getEntity() {
		if (entity == null) {
			entity = service.fetch(projectId);
		}
		return entity;
	}

}
