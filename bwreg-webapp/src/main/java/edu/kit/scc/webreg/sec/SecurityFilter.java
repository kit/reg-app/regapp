/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.sec;

import java.io.IOException;
import java.security.Security;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.MDC;

import edu.kit.scc.webreg.bootstrap.ApplicationConfig;
import edu.kit.scc.webreg.entity.AdminUserEntity;
import edu.kit.scc.webreg.entity.RoleEntity;
import edu.kit.scc.webreg.service.AdminUserService;
import edu.kit.scc.webreg.service.RoleService;
import edu.kit.scc.webreg.service.identity.EmailCheckService;
import edu.kit.scc.webreg.service.reg.PasswordUtil;
import edu.kit.scc.webreg.service.twofa.TwoFaException;
import edu.kit.scc.webreg.service.twofa.TwoFaService;
import edu.kit.scc.webreg.session.HttpRequestContext;
import edu.kit.scc.webreg.session.SessionManager;
import edu.kit.scc.webreg.util.StringIds;
import edu.kit.scc.webreg.util.ViewIds;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import net.shibboleth.shared.servlet.impl.HttpServletRequestResponseContext;

@Named
@WebFilter(urlPatterns = { "/*" })
public class SecurityFilter implements Filter {

	@Inject
	private Logger logger;

	@Inject
	private SessionManager session;

	@Inject
	private AccessChecker accessChecker;

	@Inject
	private RoleService roleService;

	@Inject
	private EmailCheckService emailCheckService;

	@Inject
	private AdminUserService adminUserService;

	@Inject
	private TwoFaService twoFaService;

	@Inject
	private ApplicationConfig appConfig;

	@Inject
	private PasswordUtil passwordUtil;

	@Inject
	private RedirectMap redirectMap;

	@Inject
	private HttpRequestContext httpRequestContext;

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		httpRequestContext.setHttpServletRequest(request);

		// OpenSAML has it's own HttpContext
		HttpServletRequestResponseContext.loadCurrent(request, response);

		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding("UTF-8");
		}

		MDC.put(StringIds.ipAddr, request.getRemoteAddr());

		String context = request.getServletContext().getContextPath();
		String path = request.getRequestURI().substring(context.length());

		HttpSession httpSession = request.getSession(false);

		if (logger.isTraceEnabled())
			logger.trace("Prechain Session is: {}", httpSession);

		if (isPathAccessibleWithoutLogin(path, session, httpSession)) {
			chain.doFilter(servletRequest, servletResponse);
			return;
		}

		// Check for login-required paths (admin, rest, etc.)
		if (isSessionValid(httpSession, session)) {
			if (path.startsWith(StringIds.ADMIN)) {
				processAdminLogin(path, request, response, chain);
				return;
			} else if (path.startsWith(StringIds.REST_DIRECT_AUTH)) {
				processDirectAuth(path, request, response, chain);
				return;
			} else if (path.startsWith(StringIds.REST)) {
				processRestLogin(path, request, response, chain);
				return;
			}
		}

		if (checkSessionStatus(session)) {

			MDC.put(StringIds.userId, "" + session.getIdentityId());

			Set<RoleEntity> roles = new HashSet<RoleEntity>(roleService.findByIdentityId(session.getIdentityId()));
			session.addRoles(roles);
			
			 // Handle redirect pages first
			
			if (path.startsWith("/r/")) {
				response.sendRedirect(redirectMap.resolveRedirect(path));
			} else {
				if (accessChecker.check(path, roles)) {
					request.setAttribute(StringIds.IDENTITY_ID, session.getIdentityId());

					if (path.startsWith(ViewIds.TWOFA_LOGIN)) {
						handling2Fa(servletRequest, servletResponse, chain, request, response);
					} else if (path.startsWith(ViewIds.IMPORTANT)) {
						handling2FaImportant(servletRequest, servletResponse, chain, request, response);
					} else {

						checkEMailValidity(path, servletRequest, servletResponse, chain, request, response);
					}
				} else {
					
					 // A user is logged in, but he may not access the requested path, due to missing roles
					 
					response.sendError(HttpServletResponse.SC_FORBIDDEN, "Not allowed");
				}

			}
		} else {
			
			 // User is not logged in yet, send to discovery page
			
			logger.debug("User from {} not logged in. Redirecting to welcome page", request.getRemoteAddr());

			session.setOriginalIdpEntityId(request.getParameter("idp"));
			session.setOriginalRequestPath(getFullURL(request));
			response.sendRedirect(ViewIds.DISCOVERY_LOGIN);
		}

		if (logger.isTraceEnabled()) {
			httpSession = request.getSession(false);
			logger.trace("Postchain Session is: {}", httpSession);
		}

		MDC.remove(StringIds.ipAddr);
		MDC.remove(StringIds.userId);
	}
	
	public boolean isSessionValid(HttpSession httpSession, SessionManager session) {
	    if (httpSession == null || !session.isLoggedIn()) {
	        return true;
	    }
	    return false;
	}

	
	public boolean checkSessionStatus(SessionManager session) {
	    if (session != null && session.isLoggedIn()) {
	        return true; 
	    }
	    return false;
	}
	
	public boolean isPathAccessibleWithoutLogin(String path, SessionManager session, HttpSession httpSession) {
		if (isAccessibleWithoutLogin(path)) {
			return true;
		}

		if ((path.startsWith(StringIds.REGISTER) && session != null && session.getIdentityId() == null)
				|| (path.startsWith(StringIds.IDP_DEBUG_LOGIN) && httpSession != null)) {
			return true;
		}

		return false;
	}

	private Boolean isAccessibleWithoutLogin(String path) {
		return StringIds.PATHS.stream().anyMatch(path::startsWith);
	}

	/*
	 * Pages which require 2fa if there is an active token are handled here
	 */
	public void handling2Fa(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		long elevationTime = 5L * 60L * 1000L;
		if (appConfig.getConfigValue(StringIds.ELEVATION_TIME) != null) {
			elevationTime = Long.parseLong(appConfig.getConfigValue(StringIds.ELEVATION_TIME));
		}

		if (session.getTwoFaElevation() != null
				&& (System.currentTimeMillis() - session.getTwoFaElevation().toEpochMilli()) < elevationTime) {
			
			// User already elevated
			chain.doFilter(servletRequest, servletResponse);
		} else {
			try {
				if (twoFaService.hasActiveTokenById(session.getIdentityId())) {
					logger.debug("User from {} not elevated. Redirecting to twofa login page", request.getRemoteAddr());
					session.setOriginalRequestPath(getFullURL(request));
					request.getServletContext().getRequestDispatcher(ViewIds.TWOFA_LOGIN).forward(servletRequest,
							servletResponse);
				} else {
					// User has no active tokens, show page anyway
					chain.doFilter(servletRequest, servletResponse);
				}
			} catch (TwoFaException e) {
				logger.warn("Cannot communicate with twofa server", e);
				throw new ServletException("There is a problem with 2fa", e);
			}
		}
	}

	// Pages which always require 2fa are handled here
	
	public void handling2FaImportant(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		long elevationTime = 5L * 60L * 1000L;
		if (appConfig.getConfigValue(StringIds.ELEVATION_TIME) != null) {
			elevationTime = Long.parseLong(appConfig.getConfigValue(StringIds.ELEVATION_TIME));
		}

		if (session.getTwoFaElevation() != null
				&& (System.currentTimeMillis() - session.getTwoFaElevation().toEpochMilli()) < elevationTime) {
			// user already elevated
			chain.doFilter(servletRequest, servletResponse);
		} else {
			logger.debug("User from {} not elevated. Redirecting to twofa login page", request.getRemoteAddr());
			session.setOriginalRequestPath(getFullURL(request));
			response.sendRedirect(ViewIds.TWOFA_LOGIN);
		}
	}

	// Access is allowed, check e-mail address validity
	
	public void checkEMailValidity(String path, ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain chain, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		if (path.startsWith(ViewIds.EMAIL_ADDRESSES) || emailCheckService.checkForValidEmail(session.getIdentityId())) {
			chain.doFilter(servletRequest, servletResponse);
		} else {
			logger.debug("User from {} has no primary e-mail address. Redirecting to email-address page",
					request.getRemoteAddr());
			session.setOriginalRequestPath(getFullURL(request));
			response.sendRedirect(ViewIds.EMAIL_ADDRESSES);
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		if (Security.getProvider(StringIds.BC) == null) {
			logger.info("Register bouncy castle crypto provider");
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		}
	}

	private void processAdminLogin(String path, HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		processHttpLogin(path, request, response, chain, true);
	}

	private void processRestLogin(String path, HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		processHttpLogin(path, request, response, chain, false);
	}

	private void processHttpLogin(String path, HttpServletRequest request, HttpServletResponse response,
			FilterChain chain, boolean setRoles) throws IOException, ServletException {

		String auth = request.getHeader(StringIds.AUTHORIZATION);
		if (auth != null && auth.startsWith("Basic ")) {

			String credentialsString = new String(Base64.decodeBase64(auth.substring(6).getBytes()));
			String[] credentials = StringUtils.split(credentialsString, ":", 2);

			if (credentials.length == 2) {
				AdminUserEntity adminUser = adminUserService.findByUsername(credentials[0]);

				if (adminUser != null && passwordsMatch(adminUser.getPassword(), credentials[1])) {

					List<RoleEntity> roleList = adminUserService.findRolesForUserById(adminUser.getId());
					Set<RoleEntity> roles = new HashSet<RoleEntity>(roleList);

					if (setRoles && session != null)
						session.addRoles(roles);

					if (accessChecker.check(path, roles)) {
						request.setAttribute(StringIds.ADMIN_USER_ID, adminUser.getId());
						chain.doFilter(request, response);
						return;
					} else
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "Not allowed");

					return;
				}
			}
		}

		response.setHeader("WWW-Authenticate", "Basic realm=\"Admin Realm\"");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.getWriter().write("<html><body>Please <a href='/'>login</a> as regular user.</body></html>");

	}

	private void processDirectAuth(String path, HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		if (appConfig.getConfigValue(StringIds.DIRECT_AUTH_ALLOW) == null) {
			logger.info("Denying direct-auth from {}", request.getRemoteAddr());
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}

		String directAuthAllow = appConfig.getConfigValue(StringIds.DIRECT_AUTH_ALLOW);
		if (!request.getRemoteAddr().matches(directAuthAllow)) {
			logger.info("Denying direct-auth from {}. Does not match.", request.getRemoteAddr());
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}

		String auth = request.getHeader(StringIds.AUTHORIZATION);
		if (auth != null && auth.startsWith("Basic ")) {
			String credentialsString = new String(Base64.decodeBase64(auth.substring(6).getBytes()));
			String[] credentials = StringUtils.split(credentialsString, ":", 2);

			if (credentials.length == 2) {
				request.setAttribute(StringIds.DIRECT_USER_ID, credentials[0]);
				request.setAttribute(StringIds.DIRECT_USER_PW, credentials[1]);
				chain.doFilter(request, response);
				return;
			}
		}

		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Basic realm=\"Admin Realm\"");

	}

	private boolean passwordsMatch(String password, String comparePassword) {
		if (password == null || comparePassword == null)
			return false;
		if (password.matches(StringIds.PASSW_REGEXP)) {
			return passwordUtil.comparePassword(comparePassword, password);
		} else {
			return comparePassword.equals(password);
		}
	}

	private String getFullURL(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder(request.getRequestURI());
		String query = request.getQueryString();

		if (query != null) {
			sb.append('?').append(query);
		}
		return sb.toString();
	}
}
