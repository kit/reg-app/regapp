package edu.kit.scc.webreg.util;

import java.util.Arrays;
import java.util.List;

public class StringIds {
	
	/*
	 * Used in the SecurityFilter.java file
	 * */
		
	public static final String ADMIN_USER_ID = "_admin_user_id";
	public static final String IDENTITY_ID = "_identity_id";
	public static final String DIRECT_USER_ID = "_direct_user_id";
	public static final String DIRECT_USER_PW = "_direct_user_pw";
		
	
	public static List<String> PATHS = Arrays.asList("/resources/", "/jakarta.faces.resource/", "/javax.faces.resource/", 
			"/welcome/", "/Shibboleth.sso/", "/saml/", "/logout/", "/error/", "/oidc/", "/rpoidc/", "/rpoauth/", "/ferest/", 
			"/rest/otp/simplecheck", "/rest/icon-cache", "/favicon.ico");

	public static final String REGISTER = "/register/";
	public static final String IDP_DEBUG_LOGIN = "/idp-debug-login/";
	
	public static final String ADMIN = "/admin";
	public static final String REST_DIRECT_AUTH = "/rest/direct-auth";
	public static final String REST = "/rest";
	
	public static final String ipAddr = "ipAddr";
	public static final String userId = "userId";
	public static final String ELEVATION_TIME = "elevation_time";
	public static final String BC = "BC";
	public static final String PASSW_REGEXP = "^\\{(.+)\\|(.+)\\|(.+)\\}$";	
	public static final String DIRECT_AUTH_ALLOW = "direct_auth_allow";
	public static final String AUTHORIZATION = "Authorization";
	public static final String USER_SELF = "user-self";
	
	
		
	

}
