/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.bean.project;

import static edu.kit.scc.webreg.dao.ops.RqlExpressions.equal;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectAdminType;
import edu.kit.scc.webreg.entity.project.ProjectEntity;
import edu.kit.scc.webreg.entity.project.ProjectIdentityAdminEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipType;
import edu.kit.scc.webreg.exc.NotAuthorizedException;
import edu.kit.scc.webreg.service.identity.IdentityService;
import edu.kit.scc.webreg.service.project.LocalProjectService;
import edu.kit.scc.webreg.service.project.ProjectService;
import edu.kit.scc.webreg.session.SessionManager;
import edu.kit.scc.webreg.util.FacesMessageGenerator;
import edu.kit.scc.webreg.util.LoadFromAppConfig;
import edu.kit.scc.webreg.util.ViewIds;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ComponentSystemEvent;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.Column;
import edu.kit.scc.webreg.util.LoadFromAppConfig;


@Named
@ViewScoped
public class UserSubprojectCreateNewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SessionManager session;

	@Inject
	private LocalProjectService localProjectService;

	@Inject
	private ProjectService projectService;
	
	@Inject
	private IdentityService identityService;
	
	@Inject
	private LoadFromAppConfig config;
	
	@Inject
	private FacesMessageGenerator messageGenerator;

	private IdentityEntity identity;
	private LocalProjectEntity entity;

	private Boolean selfMember;
	private Long parentId;
	private LocalProjectEntity parentProject;
	
	private ProjectIdentityAdminEntity adminIdentity;
	private List<ProjectIdentityAdminEntity> adminList;
	
	public void preRenderView(ComponentSystemEvent ev) {
		if (! getParentProject().getSubProjectsAllowed()) {
			throw new NotAuthorizedException("Subpropjects are not allowed for this project");
		}
		
		for (ProjectIdentityAdminEntity a : getAdminList()) {
			if (a.getIdentity().getId().equals(session.getIdentityId())) {
				adminIdentity = a;
				break;
			}
		}

		if (adminIdentity == null) {
			throw new NotAuthorizedException("Not authorized");
		} else {
			if (! adminIdentity.getType().equals(ProjectAdminType.OWNER)) {
				throw new NotAuthorizedException("Not authorized. You need Owner rights on the parent project.");
			}
		}
	}

	public IdentityEntity getIdentity() {
		if (identity == null) {
			identity = identityService.fetch(session.getIdentityId());
		}
		return identity;
	}

	public void save() {

		Column projectEntityDbColumnAnnotation = null;

		try {
			projectEntityDbColumnAnnotation = ProjectEntity.class.getDeclaredField("name")
					.getDeclaredAnnotation(Column.class);

		} catch (NoSuchFieldException | SecurityException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
			return;
		}
	
		if (entity.getName().length() > projectEntityDbColumnAnnotation.length()) {
			messageGenerator.addResolvedErrorMessage("failure", "project.local_project.create_edit_name_length_details", true);
			return;
		}
		
		Number existingProjectShortNames = localProjectService.countAll(equal(LocalProjectEntity_.shortName, entity.getShortName()));
		Number existingProjectGroupNames = localProjectService.countAll(equal(LocalProjectEntity_.groupName, entity.getGroupName()));
		
		if (existingProjectShortNames.intValue() > 0 || existingProjectGroupNames.intValue() > 0 ) {
			messageGenerator.addResolvedErrorMessage("failure", "project.local_project.create_edit_name_unique_details",
					true);
			return;

		}

		entity.setParentProject(getParentProject());
		
		if (config.getProjectValdity() != 0L)
			entity.setProjectValidity(new Date(System.currentTimeMillis() + config.getProjectValdity()));
		else 
			entity.setProjectValidity(null);
				
		entity = localProjectService.save(entity, getIdentity().getId());

		if (entity != null) {
			messageGenerator.addResolvedInfoMessage("success", "project.local_project.create_edit_name_success_detail",
					true);
		}

		if (getSelfMember()) {
			
			ProjectMembershipEntity pme = new ProjectMembershipEntity();
			pme.setMembershipType(ProjectMembershipType.MEMBER);
			
			if (config.getMemberShipValdity() != 0L)
				pme.setMembershipValidity(new Date(System.currentTimeMillis() + config.getMemberShipValdity()));
			else
				pme.setMembershipValidity(null);
			
			projectService.addProjectMember(entity, getIdentity(), pme, "idty-" + getIdentity().getId());
			messageGenerator.addResolvedInfoMessage("success", "project.local_project.create_edit_name_add_member",
					true);
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(ViewIds.PROJECT_LOCAL_SHOW + "?id=" + entity.getId());
		} catch (IOException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
		}
	}

	
	public void cancel() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(ViewIds.PROJECT_LOCAL_INDEX);
		} catch (IOException e) {
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
		}

	}

	public LocalProjectEntity getEntity() {
		if (entity == null) {
			entity = localProjectService.createNew();
		}
		return entity;
	}

	public void setEntity(LocalProjectEntity entity) {
		this.entity = entity;
	}

	public Boolean getSelfMember() {
		if (selfMember == null) {
			selfMember = Boolean.FALSE;
		}
		return selfMember;
	}

	public void setSelfMember(Boolean selfMember) {
		this.selfMember = selfMember;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public LocalProjectEntity getParentProject() {
		if (parentProject == null) {
			parentProject = localProjectService.fetch(getParentId());
		}
		return parentProject;
	}
	
	public List<ProjectIdentityAdminEntity> getAdminList() {
		if (adminList == null) {
			adminList = projectService.findAdminsForProject(getParentProject());
		}
		return adminList;
	}
}
