package edu.kit.scc.webreg.util;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.kit.scc.webreg.bootstrap.ApplicationConfig;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;


@Named("loadFromAppConfig")
@ApplicationScoped

public class LoadFromAppConfig  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ApplicationConfig appConfig;
	
	public long getMemberShipValdity() {

		String membershipValidityFromAppConfig = appConfig.getConfigValue("membership_validity");
		String regex = "^\\d{11}$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(membershipValidityFromAppConfig);

		if (matcher.matches()) {
			long membershipValidityMs;
			try {
				membershipValidityMs = Long.parseLong(membershipValidityFromAppConfig);
				return membershipValidityMs;

			} catch (NumberFormatException e) {
				throw new IllegalArgumentException(
						"Invalid format for membership_validity in config: " + membershipValidityFromAppConfig, e);
			}
		} else
			return 0L;

	}
	
	public long getProjectValdity() {

		String projectValidityFromAppConfig = appConfig.getConfigValue("project_validity");
		String regex = "^\\d{11}$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(projectValidityFromAppConfig);

		if (matcher.matches()) {
			long projectValidityMs;
			try {
				projectValidityMs = Long.parseLong(projectValidityFromAppConfig);
				return projectValidityMs;

			} catch (NumberFormatException e) {
				throw new IllegalArgumentException(
						"Invalid format for project_validity in config: " + projectValidityFromAppConfig, e);
			}
		} else
			return 0L;

	}
	

}

