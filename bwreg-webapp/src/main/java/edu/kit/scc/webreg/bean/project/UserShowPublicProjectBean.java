/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Haykuhi Musheghyan
 ******************************************************************************/
package edu.kit.scc.webreg.bean.project;

import java.io.Serializable;
import java.util.Date;

import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Getter;
import lombok.Setter;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity_;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity;
import edu.kit.scc.webreg.entity.project.LocalProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectMembershipEntity;
import edu.kit.scc.webreg.entity.project.ProjectMembershipType;
import edu.kit.scc.webreg.service.identity.IdentityService;
import edu.kit.scc.webreg.service.project.LocalProjectService;
import edu.kit.scc.webreg.service.project.ProjectService;
import edu.kit.scc.webreg.session.SessionManager;
import edu.kit.scc.webreg.util.FacesMessageGenerator;
import edu.kit.scc.webreg.util.LoadFromAppConfig;

@Named
@ViewScoped
public class UserShowPublicProjectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SessionManager session;

	@Inject
	private LocalProjectService service;

	@Inject
	private ProjectService projectService;

	@Inject
	private FacesMessageGenerator messageGenerator;

	@Inject
	private IdentityService identityService;
	
	@Inject
	private LoadFromAppConfig config;

	@Setter
	private LocalProjectEntity entity;

	private IdentityEntity identity;

	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String reason;

	public LocalProjectEntity getEntity() {
		if (entity == null) {
			entity = service.findByIdWithAttrs(id, LocalProjectEntity_.projectServices, ProjectEntity_.childProjects);
		}
		return entity;
	}

	public String getStatus() {
		ProjectMembershipEntity isMember = projectService.findByIdentityAndProject(getIdentity(), getEntity());

		if (isMember == null) {
			return "NOT APPLIED";
		}

		return isMember.getMembershipType() == ProjectMembershipType.NOTMEMBER ? "DECLINED"
				: isMember.getMembershipType().toString();
	}

	public String getApprovalDenyReason() {
		ProjectMembershipEntity isMember = projectService.findByIdentityAndProject(getIdentity(), getEntity());
		if (isMember != null)
			return isMember.getReasonToApproveDenyMembership();
		return "";
	}

	public void applyForExistingProjects() {
		
		identity = getIdentity();
		entity = getEntity();
		reason = getReason();

		ProjectMembershipEntity isMember = projectService.findByIdentityAndProject(identity, entity);

		if (reason == null || reason.isEmpty()) {
			messageGenerator.addResolvedErrorMessage("mandatoy", "mandatoy_details", true);
			return;
		}
		

		if (isMember == null) {
			
			ProjectMembershipEntity pme = new ProjectMembershipEntity();
			pme.setReasonToApplyForMembership(reason);
			pme.setMembershipType(ProjectMembershipType.APPLICANT);
			
			if (config.getMemberShipValdity() != 0L)
				pme.setMembershipValidity(new Date(System.currentTimeMillis() + config.getMemberShipValdity()));
			else
				pme.setMembershipValidity(null);
			
			projectService.addProjectMember(entity, identity, pme, "idty-" + session.getIdentityId());
			messageGenerator.addResolvedInfoMessage("project.public_project.apply",
					"project.public_project.apply_details", true);
		}

		else if (isMember.getMembershipType().equals(ProjectMembershipType.NOTMEMBER)) {
			
			isMember.setReasonToApplyForMembership(reason);
			isMember.setMembershipType(ProjectMembershipType.APPLICANT);
			
			if (config.getMemberShipValdity() != 0L)
				isMember.setMembershipValidity(new Date(System.currentTimeMillis() + config.getMemberShipValdity()));
			else
				isMember.setMembershipValidity(null);	
			
			projectService.updateProjectMemberStatus(isMember, "idty-" + session.getIdentityId());
			messageGenerator.addResolvedInfoMessage("project.public_project.apply",
					"project.public_project.apply_details", true);
		}

		identity = null;
		entity = null;
		reason = null;
	}
	

	public IdentityEntity getIdentity() {
		if (identity == null) {
			identity = identityService.findByIdWithAttrs(session.getIdentityId(), IdentityEntity_.emailAddresses);
		}
		return identity;
	}

	public void deleteProjectMembership() {
		ProjectMembershipEntity isMember = projectService.findByIdentityAndProject(getIdentity(), getEntity());
		if (isMember != null) {
			projectService.removeProjectMember(isMember, "idty-" + session.getIdentityId());
			messageGenerator.addResolvedInfoMessage("project.public_project.delete",
					"project.public_project.delete_details", true);
		}
		identity = null;
		entity = null;
		reason = null;
	}

}
