/*******************************************************************************
 * Copyright (c) 2014 Michael Simon.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Michael Simon - initial
 ******************************************************************************/
package edu.kit.scc.webreg.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import edu.kit.scc.webreg.entity.identity.EmailAddressStatus;
import edu.kit.scc.webreg.entity.identity.IdentityEmailAddressEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import edu.kit.scc.webreg.entity.identity.IdentityEntity_;
import edu.kit.scc.webreg.exc.VerificationException;
import edu.kit.scc.webreg.service.identity.IdentityEmailAddressService;
import edu.kit.scc.webreg.service.identity.IdentityService;
import edu.kit.scc.webreg.session.SessionManager;
import edu.kit.scc.webreg.util.FacesMessageGenerator;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.PreRenderViewEvent;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.mail.internet.AddressException;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class EmailAddressesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SessionManager session;

	@Inject
	private IdentityService identityService;

	@Inject
	private IdentityEmailAddressService service;

	@Inject
	private FacesMessageGenerator messageGenerator;

	private IdentityEntity identity;

	@Email
	@Getter
	@Setter
	private String addEmailAddress;

	@Email
	@Getter
	@Setter
	private String inputEmailAddress;

	@Getter
	@Setter
	private String token;

	@Setter
	private List<IdentityEmailAddressEntity> primaryEmailList;

	@Setter
	private IdentityEmailAddressEntity chosenPrimary;

	public Boolean isBackButtonVisable() {
		return session.getOriginalRequestPath() == null;
	}

	public void preRenderView(PreRenderViewEvent ev) {

		IdentityEmailAddressEntity choosenEmail = getChosenPrimary();

		if (getToken() != null) {
			if (choosenEmail != null) {
				checkVerification();
			} else {
				checkVerificationAndSetAsPrimary();
			}
		}
	}

	public void redirectToOriginalPage() {
		try {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(session.getOriginalRequestPath());
			session.setOriginalRequestPath(null);
		} catch (IOException e) {
			throw new IllegalArgumentException("redirect failed");
		}
	}

	public Boolean isContinueButtonVisable() {
		return session.getOriginalRequestPath() != null && getIdentity().getPrimaryEmail() != null;
	}

	public void addEmailAddress() {
		if (addEmailAddress == null) {
			messageGenerator.addResolvedErrorMessage("email_addresses.set_notification",
					"email_addresses.set_notification_details", true);
			return;
		}

		boolean isEmailExists = getIdentity().getEmailAddresses().stream()
				.anyMatch(t -> addEmailAddress.equals(t.getEmailAddress()));

		if (isEmailExists) {
			messageGenerator.addResolvedErrorMessage("email_addresses_already_exists",
					"email_addresses_already_exists_deatils", true);
		} else {
			try {
				service.addEmailAddress(getIdentity(), addEmailAddress, "idty-" + session.getIdentityId());
				setAddEmailAddress(null);
				identity = null;
				messageGenerator.addResolvedInfoMessage("email_addresses.set_success",
						"email_addresses.set_success_details", true);
			} catch (AddressException e) {
				messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
			}
		}
	}

	public void setAnEmailAddress() {
		if (inputEmailAddress == null) {
			messageGenerator.addResolvedErrorMessage("email_addresses.set_notification",
					"email_addresses.set_notification_details", true);
			return;
		}

		boolean isEmailExists = getIdentity().getEmailAddresses().stream()
				.anyMatch(t -> inputEmailAddress.equals(t.getEmailAddress()));

		if (isEmailExists) {
			messageGenerator.addResolvedErrorMessage("email_addresses_already_exists",
					"email_addresses_already_exists_deatils", true);
		} else {
			try {
				service.addEmailAddress(getIdentity(), inputEmailAddress, "idty-" + session.getIdentityId());
				setAddEmailAddress(null);
				identity = null;
				messageGenerator.addResolvedInfoMessage("email_addresses.set_success",
						"email_addresses.set_success_details", true);
			} catch (AddressException e) {
				messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten", e.toString());
			}
		}
	}

	public void deleteEmailAddress(IdentityEmailAddressEntity address) {
		if (address != null) {
			service.deleteEmailAddress(address, "idty-" + session.getIdentityId());
			identity = null;
			chosenPrimary = null;
			primaryEmailList = null;
			messageGenerator.addResolvedInfoMessage("email_addresses_deleted", "email_addresses_deleted_details", true);
		} else
			messageGenerator.addErrorMessage("Ein Fehler ist aufgetreten: kann nicht gelöscht werden");

	}

	public void resendVerificationToken(IdentityEmailAddressEntity address) {
		service.redoVerification(address, "idty-" + session.getIdentityId());
		identity = null;
	}

	public void setPrimaryEmailAddress() {
		service.setPrimaryEmailAddress(getChosenPrimary(), "idty-" + session.getIdentityId());
		reorderEmails();
		identity = null;
		chosenPrimary = null;
		messageGenerator.addResolvedInfoMessage("email_addresses.set_primary_email_success",
				"email_addresses.set_primary_email_success_detail", true);

	}

	public void reorderEmails() {
		if (chosenPrimary != null) {
			// Move the chosen email to the top of the list
			primaryEmailList = new ArrayList<>(primaryEmailList);
			primaryEmailList.remove(chosenPrimary);
			primaryEmailList.add(0, chosenPrimary);

			// Except of the preferred email address, sort the rest alphabetically in the
			// list
			Collections.sort(primaryEmailList.subList(1, primaryEmailList.size()),
					(o1, o2) -> o1.getEmailAddress().compareTo(o2.getEmailAddress()));

		}
	}

	public void checkVerification() {
		try {
			service.checkVerification(getIdentity(), getToken(), "idty-" + session.getIdentityId());
			token = null;
			identity = null;
			chosenPrimary = null;
			primaryEmailList = null;
			messageGenerator.addResolvedInfoMessage("email_addresses.verification_success",
					"email_addresses.verification_success_detail", true);
		} catch (VerificationException e) {
			messageGenerator.addResolvedErrorMessage("email_addresses." + e.getMessage());
		}
	}

	public void checkVerificationAndSetAsPrimary() {

		Set<IdentityEmailAddressEntity> existingEmailAddresses;

		try {
			service.checkVerification(getIdentity(), getToken(), "idty-" + session.getIdentityId());
			existingEmailAddresses = getIdentity().getEmailAddresses();
			if (!existingEmailAddresses.isEmpty()) {
				service.setPrimaryEmailAddress(existingEmailAddresses.stream().findAny().get(),
						"idty-" + session.getIdentityId());
			}
			token = null;
			identity = null;
			messageGenerator.addResolvedInfoMessage("email_addresses.set_success",
					"email_addresses.verification_success_and_set_detail", true);
		} catch (VerificationException e) {
			messageGenerator.addResolvedErrorMessage("email_addresses." + e.getMessage());
		}
	}

	public IdentityEntity getIdentity() {
		if (identity == null) {
			identity = identityService.findByIdWithAttrs(session.getIdentityId(), IdentityEntity_.emailAddresses);
		}
		return identity;
	}

	public List<IdentityEmailAddressEntity> getPrimaryEmailList() {

		if (primaryEmailList == null) {
			primaryEmailList = new ArrayList<>(getIdentity().getEmailAddresses().stream()
					.filter(e -> !EmailAddressStatus.UNVERIFIED.equals(e.getEmailStatus())).toList());
		}

		if (!primaryEmailList.isEmpty())
			reorderEmails();

		return primaryEmailList;

	}

	public IdentityEmailAddressEntity getChosenPrimary() {
		if (chosenPrimary == null) {
			chosenPrimary = getIdentity().getPrimaryEmail();
		}
		return chosenPrimary;
	}

}
