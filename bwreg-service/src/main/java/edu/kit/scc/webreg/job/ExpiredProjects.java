package edu.kit.scc.webreg.job;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.kit.scc.webreg.entity.project.ProjectEntity;
import edu.kit.scc.webreg.entity.project.ProjectEntity_;
import edu.kit.scc.webreg.entity.project.ProjectStatus;
import edu.kit.scc.webreg.service.project.ProjectService;

import static edu.kit.scc.webreg.dao.ops.PaginateBy.unlimited;
import static edu.kit.scc.webreg.dao.ops.RqlExpressions.lessThan;
import static edu.kit.scc.webreg.dao.ops.SortBy.ascendingBy;
import static edu.kit.scc.webreg.dao.ops.RqlExpressions.isNull;
import static edu.kit.scc.webreg.dao.ops.RqlExpressions.and;

public class ExpiredProjects extends AbstractExecutableJob {

	private static final long serialVersionUID = 1L;

	@Override
	public void execute() {
		Logger logger = LoggerFactory.getLogger(ExpiredProjects.class);

		try {
			InitialContext ic = new InitialContext();

			ProjectService projectService = (ProjectService) ic.lookup(
					"global/bwreg/bwreg-service/ProjectServiceImpl!edu.kit.scc.webreg.service.project.ProjectService");

			List<ProjectEntity> expiredProjects = projectService.findAllEagerly(unlimited(),
					Arrays.asList(ascendingBy(ProjectEntity_.id)),
					and(isNull(ProjectEntity_.projectStatus), lessThan(ProjectEntity_.projectValidity, new Date())));

			if (expiredProjects != null && !expiredProjects.isEmpty()) {

				for (ProjectEntity pm : expiredProjects) {
					pm.setProjectStatus(ProjectStatus.EXPIRED);
					projectService.updateProjectStatus(pm, "Expired projects");
				}

				logger.debug("Update is done");
			} else {
				logger.debug("No expired projects to delete");
			}

		} catch (NamingException e) {
			logger.warn("Could not change the status of a project: {}", e);
		}
	}

}
