package edu.kit.scc.webreg.service.identity;

import edu.kit.scc.webreg.dao.identity.IdentityDao;
import edu.kit.scc.webreg.entity.identity.IdentityEntity;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

@Stateless
public class EmailCheckService {

	@Inject
	private IdentityDao identityDao;
	
	public Boolean checkForValidEmail(Long identityId) {
		IdentityEntity identity = identityDao.fetch(identityId);
		return identity.getPrimaryEmail() != null;
	}
}
