package edu.kit.scc.webreg.job;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.kit.scc.webreg.entity.project.ProjectMembershipEntity;
import edu.kit.scc.webreg.service.project.ProjectService;

public class ExpiredProjectMembership extends AbstractExecutableJob {

	private static final long serialVersionUID = 1L;

	@Override
	public void execute() {
		Logger logger = LoggerFactory.getLogger(ExpiredProjectMembership.class);

		try {
			InitialContext ic = new InitialContext();

			ProjectService projectService = (ProjectService) ic.lookup(
					"global/bwreg/bwreg-service/ProjectServiceImpl!edu.kit.scc.webreg.service.project.ProjectService");

			List<ProjectMembershipEntity> expiredMembersList = projectService.findExpiredMembers();

			if (expiredMembersList != null && !expiredMembersList.isEmpty()) {
				for (ProjectMembershipEntity mem : expiredMembersList) {
					projectService.removeProjectMember(mem, "Expired membership");
				}
				logger.debug("Deletion is done");
			} else {
				logger.debug("No expired members to delete");
			}

		} catch (NamingException e) {
			logger.warn("Could not delete expired project memberships: {}", e);
		}
	}

}
