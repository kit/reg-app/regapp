# reg-app

Passive identity management (IDM) application, developed first as part of the bwIdm project.

[User Guide](https://gitlab.kit.edu/kit/reg-app/regapp/-/wikis/home)
